package org.seipiales;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Canvas;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;



/**
 * ViewPortMovimiento.java <BR>
 * Author: Sandra Ipiales
 * Cono con 2 ViewPort
 */
public class ViewPortMovimiento extends JFrame implements KeyListener {
    
    private static GLU glu;
    private static GL gl;
    private static GLUT glut;
    private static GLCanvas canvas;
    private static int ancho, alto;
    //Variables de rotacion 
    private static float rotarX=0;
    private static float rotarY=0;
    private static float rotarZ=0;
    
    //Variables para la traslacion 
    private static float trasladaX=0;
    private static float trasladaY=0;
    private static float trasladaZ=0;
    
    public ViewPortMovimiento(){
        setSize(800,700);
        setLocationRelativeTo(null);
        setTitle("View Port Movimiento");
        setResizable(false);
        GraphicsListener listener= new GraphicsListener();
        alto= this.getHeight();
        ancho= this.getWidth();
        
        canvas= new GLCanvas();
        gl= canvas.getGL();
        glut = new GLUT();
        
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);
        
        Animator animator= new Animator(canvas);
        animator.start();
        addKeyListener(this);
        
    }
    public static void main(String[] args) {
        ViewPortMovimiento frame= new ViewPortMovimiento();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

 
    public class GraphicsListener implements GLEventListener{
    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();

        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
//        gl.glClearColor(0.4576f, 0.208f, 0.52f, 0);
        gl.glClearColor(0.3419f, 0.0888f, 0.37f, 0);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
         // Dibuja Cono Miniatura
        gl.glPushMatrix();
         gl.glViewport(40, alto/2+100, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glTranslated(trasladaX, trasladaY, trasladaZ);
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.85f, 0.557f, 0.051f);
        glut.glutWireCone(2, 4, 40, 20);
        gl.glPopMatrix();
        // Dibuja Cono 
        gl.glPushMatrix();
        gl.glViewport((ancho/4)+40, 0, ancho-(ancho/4), alto);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        
        gl.glTranslated(trasladaX, trasladaY, trasladaZ);
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        
        gl.glColor3f(0.85f, 0.557f, 0.051f);
        glut.glutWireCone(2, 4, 40, 20);
        gl.glPopMatrix();
        
       
        
        gl.glFlush();
    }
       public void init(GLAutoDrawable drawable) {
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
    
    }

    public void keyTyped(KeyEvent ke) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode()==KeyEvent.VK_RIGHT){   
            trasladaX+=.1f;
            rotarX+=3.0f;
            System.out.println("Valor en la traslacion de X: " + trasladaX);
        }
        if(e.getKeyCode()==KeyEvent.VK_LEFT){
               trasladaX-=.1f; 
               rotarX-=3.0f;
               System.out.println("Valor en la traslacion de X: " + trasladaX);
           
        }
        if(e.getKeyCode()==KeyEvent.VK_UP){
                 trasladaY+=.1f;
                 rotarY+=3.0f;
                 System.out.println("Valor en la traslacion de Y: " + trasladaY);
            
           
        }
        if(e.getKeyCode()==KeyEvent.VK_DOWN){
                trasladaY-=.1f;
                rotarY-=3.0f;
                 System.out.println("Valor en la traslacion de Y: " + trasladaY);
            
        }
        if(e.getKeyCode()==KeyEvent.VK_K){
                 trasladaZ+=.1f;
                 rotarZ+=3.0f;
                 System.out.println("Valor en la traslacion de Z: " + trasladaZ);
            
           
        }
        if(e.getKeyCode()==KeyEvent.VK_L){
                trasladaZ-=.1f;
                rotarZ-=3.0f;
                 System.out.println("Valor en la traslacion de Z: " + trasladaZ);
            
        }
        if(e.getKeyCode() == KeyEvent.VK_X){
                rotarX+=1.0f;
            System.out.println("Valor de X en la rotacion: " + rotarX);
        }

        if(e.getKeyCode() == KeyEvent.VK_C){
            rotarX-=1.0f;
            System.out.println("Valor de Y en la rotacion: " + rotarX);
        }
        if(e.getKeyCode() == KeyEvent.VK_S){
            rotarY+=1.0f;
            System.out.println("Valor de Y en la rotacion: " + rotarY);
        }
        if(e.getKeyCode() == KeyEvent.VK_D){
            rotarY-=1.0f;
            System.out.println("Valor de Y en la rotacion: " + rotarY);
        }
        if(e.getKeyCode() == KeyEvent.VK_W){
            rotarZ+=1.0f;
            System.out.println("Valor de Z en la rotacion: " + rotarZ);
        }
        if(e.getKeyCode() == KeyEvent.VK_E){
            rotarZ-=1.0f;
            System.out.println("Valor de Z en la rotacion: " + rotarZ);
        }
       
         if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
            trasladaX=0;
            trasladaY=0;
            trasladaZ=0;            
            rotarX=0;
            rotarY=0;
            rotarZ=0;
        }

    }

    public void keyReleased(KeyEvent ke) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

