package org.seipiales;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;



/**
 * ViewPort.java <BR>
 * author: Sandra Ipiales
 * View con Solidos GLUT de OPENGL
 */
public class ViewPortFigurasGLUT extends JFrame implements KeyListener {
    static GL gl;
    static GLU glu;
    static GLUT glut;
    static GLCanvas canvas;
    static int ancho, alto; // Ancho y alto de la ventana
    
   //Variables de rotacion 
    private static float rotarX=0;
    private static float rotarY=0;
    private static float rotarZ=0;
    
    //Variables para la traslacion 
//    private static float trasladaX=0;
//    private static float trasladaY=0;
//    private static float trasladaZ=0;
    
    public ViewPortFigurasGLUT(){
        setSize(580,600);
        setLocationRelativeTo(null);
        setTitle("Varias Figuras con Viewport");
        setResizable(false);
        GraphicListener listener = new GraphicListener();
        alto= this.getHeight();
        ancho= this.getWidth();
        //Inicializamos Canvas
        canvas= new GLCanvas();
        gl= canvas.getGL();
        glut = new GLUT();
        
        /* Detecta eventos para renderizado en OPENGL, llama a init()
        Renderiza los graficos del metodo display*/
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);
        
        Animator animator= new Animator(canvas);
        animator.start();
        addKeyListener(this);
    
    }
    
    public static void main(String[] args) {
        ViewPortFigurasGLUT frame = new ViewPortFigurasGLUT();
        // Center frame
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void keyTyped(KeyEvent ke) {
    }

    public void keyPressed(KeyEvent e) {      
        if(e.getKeyCode() == KeyEvent.VK_X){
                rotarX+=1.0f;
            System.out.println("Valor de X en la rotacion: " + rotarX);
        }

        if(e.getKeyCode() == KeyEvent.VK_C){
            rotarX-=1.0f;
            System.out.println("Valor de Y en la rotacion: " + rotarX);
        }
        if(e.getKeyCode() == KeyEvent.VK_S){
            rotarY+=1.0f;
            System.out.println("Valor de Y en la rotacion: " + rotarY);
        }
        if(e.getKeyCode() == KeyEvent.VK_D){
            rotarY-=1.0f;
            System.out.println("Valor de Y en la rotacion: " + rotarY);
        }
        if(e.getKeyCode() == KeyEvent.VK_W){
            rotarZ+=1.0f;
            System.out.println("Valor de Z en la rotacion: " + rotarZ);
        }
        if(e.getKeyCode() == KeyEvent.VK_E){
            rotarZ-=1.0f;
            System.out.println("Valor de Z en la rotacion: " + rotarZ);
        }
       
         if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
            rotarX=0;
            rotarY=0;
            rotarZ=0;
        }
    }

    public void keyReleased(KeyEvent ke) {
        
    }

    
    
    public class GraphicListener implements GLEventListener{
    public void display(GLAutoDrawable arg0) {
        GL gl = arg0.getGL();
        gl.glClear(GL.GL_COLOR_BUFFER_BIT);
        gl.glClearColor(0.84f, 0.84f, 0.84f, 0.0f); //Fondo

        
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        
        //Se dibuja un cilindro
        gl.glViewport(0, (alto/2)+115, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glRotatef(10, 0, 1, 1);
        gl.glTranslatef(.5f, 0, 1.5f);
        gl.glTranslatef(0, .5f, 0);
        gl.glScalef(1, 0.5f, 1);
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.1323f, 0.4602f, 0.49f);
        glut.glutWireCylinder(1.5f, 3, 18, 18);

        
        //Se dibuja un sphere
        gl.glViewport(0, (alto/2)-30, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glRotatef(10, 0, 1, 1);
        gl.glTranslatef(.5f, 0, 1.5f);
        gl.glTranslatef(0, .4f, 0);
        gl.glScalef(1, 1, 1);
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.0968f, 0.3256f, 0.44f);
        glut.glutWireSphere(3, 10, 10);
        
        //Se dibuja un cono
        gl.glViewport(0, (alto/4)-20, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glRotatef(10, 0, 1, 1);
        gl.glTranslatef(.5f, 0, 1.5f);
        gl.glTranslatef(0, .5f, 0);
        gl.glScalef(3, 1.5f, 1); //Escalamos la figura
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.5f, 0.055f, 0.3665f);
        glut.glutWireCone(1, 1, 4, 20);
        
        //Se dibuja un dodecahedro
        gl.glViewport(0, (alto/2)-300, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glRotatef(10, 0, 1, 1);
        gl.glTranslatef(.5f, 0, 1.5f);
        gl.glTranslatef(0, .5f, 0);
        gl.glScalef(2, 2, 1); //Escalamos la figura
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.5f, 0.055f, 0.3665f);
        glut.glutWireDodecahedron();
        
        //Se dibuja un Rombododecahedro
        gl.glViewport(180, (alto/4)-15, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glRotatef(10, 0, 1, 1);
        gl.glTranslatef(.5f, 0, 1.5f);
        gl.glTranslatef(0, .5f, 0);
        gl.glScalef(4, 4, 3); //Escalamos la figura
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.414f, 0.56f, 0.1792f);
        glut.glutWireRhombicDodecahedron();
        
        //Se dibuja un cubo
        gl.glViewport(180, (alto/4)-150, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glRotatef(10, 0, 1, 1);
        gl.glTranslatef(.5f, 0, 1.5f);
        gl.glTranslatef(0, .5f, 0);
        gl.glScalef(2, 2, 2); //Escalamos la figura
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.4899f, 0.5166f, 0.69f);
        glut.glutWireCube(2f);
        
        //Se dibuja un Icosahedro
        gl.glViewport(180, (alto/2)+115, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glRotatef(10, 0, 1, 1);
        gl.glTranslatef(.5f, 0, 1.5f);
        gl.glTranslatef(0, .5f, 0);
        gl.glScalef(3, 3, 3); //Escalamos la figura
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.4899f, 0.5166f, 0.69f);
        glut.glutWireIcosahedron();
        
        //Se dibuja un Cono
        gl.glViewport(180, (alto/2)-20, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glRotatef(10, 0, 1, 1);
        gl.glTranslatef(.5f, 0, 1.5f);
        gl.glTranslatef(0, .5f, 0);
        gl.glScalef(1, 2, 1); //Escalamos la figura
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.52f, 0.65f, 0.325f);
        glut.glutWireCone(1, 2, 4, 20);
        
        //Se dibuja esfera solida
        gl.glViewport(350, (alto/2)+105, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glRotatef(10, 0, 1, 1);
        gl.glTranslatef(.5f, 0, 1.5f);
        gl.glTranslatef(0, .5f, 0);
        gl.glScalef(1, 2, 1); //Escalamos la figura
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.84f, 0.4794f, 0.3696f);
        glut.glutWireSphere(2, 25, 25);
        
        
        //Se dibuja tetera
        gl.glViewport(350, (alto/2)-30, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glRotatef(10, 0, 1, 1);
        gl.glTranslatef(.5f, 0, 1.5f);
        gl.glTranslatef(0, .5f, 0);
        gl.glScalef(1, 2, 1); //Escalamos la figura
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.6726f, 0.84f, 0.0672f);
        glut.glutWireTeapot(2);
        
        //Se dibuja tetrahedro
        gl.glViewport(350, (alto/2)-170, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glRotatef(10, 0, 1, 1);
        gl.glTranslatef(.5f, 0, 1.5f);
        gl.glTranslatef(0, .5f, 0);
        gl.glScalef(3, 3, 2); //Escalamos la figura
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.024f, 0.3584f, 0.48f);
        glut.glutWireTetrahedron();
        
        //Se dibuja Dona
        gl.glViewport(350, (alto/2)-290, ancho/4, alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 5, -5, 5, -5, 5);
        gl.glRotatef(10, 0, 1, 1);
        gl.glTranslatef(.5f, 0, 1.5f);
        gl.glTranslatef(0, .5f, 0);
        gl.glScalef(2, 2, 1); //Escalamos la figura
        gl.glRotatef(rotarX, 1f, 0, 0);
        gl.glRotatef(rotarY, 0, 1f, 0);
        gl.glRotatef(rotarZ, 0, 0, 1f);
        gl.glColor3f(0.024f, 0.3584f, 0.48f);
        glut.glutWireTorus(.5f, 1, 5, 40);
        

        gl.glFlush();
    }

        public void init(GLAutoDrawable drawable) {

    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

    }
        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

}

